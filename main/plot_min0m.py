#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 12 14:49:18 2020

@author: bulbulahmmed
This code generates line plots for data I used to NMFk code
"""

a    = np.genfromtxt("data/" + files[1], delimiter=',', skip_header=1)

W = np.genfromtxt("results/W_min0m.csv", delimiter=',')

H = np.genfromtxt("results/H_min0m.csv", delimiter=',')

fit = np.genfromtxt("results/fitness_min0m.csv", delimiter=',')

signals=["S1","S2","S3"]

fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(7.5, 6.5))

#---------------------- Plot # a --------------------------------;

plot_lines(a,colors,variables)

#---------------------- Plot # b --------------------------------;
ax2.plot(fit[:,0], fit[:,1], 'k', label="$\epsilon(k)$")
ax2.plot(fit[:,0], fit[:,2], 'r', label="Silhoutte width")
ax2.set_xlabel("Signal")
ax2.legend()
ax2.grid(linestyle='--')
ax2.set_yticks([0,1,5,10,15])
#---------------------- Plot # c --------------------------------;
ax3.plot(time, H[0,:], 'k', label=signals[0])
ax3.plot(time, H[1,:], 'g', label=signals[1])
ax3.plot(time, H[2,:], 'r', label=signals[2])
#ax3.plot(time, H[2,:], 'b', label=signals[3])
ax3.set_xlabel("Time (yr)")
ax3.set_ylabel("Signal magnitude (-)")
ax3.grid(which='both', linestyle='--')
ax3.legend()
#---------------------- Plot # d --------------------------------;
sns.heatmap(W/np.amax(W, axis=0), xticklabels=signals, yticklabels=minerals, 
                 cmap="jet", linewidth=0.25, square=True, ax=ax4)

fig.tight_layout()

fig.savefig('figures/minerals0m.pdf')


