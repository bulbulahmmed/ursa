#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 12 14:49:18 2020

@author: bulbulahmmed
This code generates plot for NMFk-CO2 sequestration work
Each plot will have four subplots
a. will include normalized data
b. Silhoutte vs Fitnees
c. W matrix as line map
d. H matrix as heatmap
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
plt.rcParams['font.size'] = 10
plt.rcParams["font.family"] = "serif"
#plt.rcParams["font.family"] = "Arial"
plt.rcParams['mathtext.it'] = 'STIXGeneral:italic'
plt.rcParams['mathtext.bf'] = 'STIXGeneral:italic:bold'
#plt.rc('text', usetex=True)
var_colors = ["k","b","r","m","c","g","y"]

min_colors = ["k","b","r","m","c","g","y"]

sp_colors = ["k","b","r","m","c","g"]

variables = ["Pressure","Temperature","pH","Void$_c$","Void$_{nc}$","Void$_t$","Aq. CO$_2$","SMCO$_2"]

species  = ["Na$^{+}$","K$^{+}$","Ca$^{2+}$","Mg$^{2+}$","HCO$_{3}^{+}$","Fe$^{2+}$"]

minerals = ["Calcite","Dolomite","Ankerite","Siderite","Quartz","Albite","Clinochlore","Illite","Kaolinite","Smectite"]

time = np.arange(1, 201, 1)

files = ["physio-chem_variables.csv","mins_0m.csv","mins_200m.csv","species_0m.csv",
         "species_200m.csv"]

def plot_lines(inputdat,colors,ax,labels,figname):
    for i in range(inputdat.shape[1]):
        ax.plot(time, inputdat[:,i]/max(inputdat[:,i]), colors[i], label=labels[i])
        ax.legend()
        ax.set_xlabel("Time (yr)")
        ax.set_ylabel("Normalized (-)")
        ax.grid(which='both', linestyle='--')
        fig.tight_layout()
        fig.savefig(figname)

exec(open('plot_vars.py').read())

exec(open('plot_min0m.py').read())

exec(open('plot_min200m.py').read())

exec(open('plot_sp0m.py').read())

exec(open('plot_sp200m.py').read())
