#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 12 14:49:18 2020

@author: bulbulahmmed
This code generates plot for NMFk-CO2 sequestration work
Each plot will have four subplots
a. will include normalized data
b. Silhoutte vs Fitnees
c. W matrix as line map
d. H matrix as heatmap
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.gridspec as gridspec
plt.rcParams['font.size'] = 14
plt.rcParams["font.family"] = "serif"
#plt.rcParams["font.family"] = "Arial"
plt.rcParams['mathtext.it'] = 'STIXGeneral:italic'
plt.rcParams['mathtext.bf'] = 'STIXGeneral:italic:bold'
#plt.rc('text', usetex=True)

colors = ['#1f77b4','#ff7f0e','#2ca02c','#d62728']
linestyles = ['solid', 'dashed', 'dashdot','dotted']
signals = ["S$_\mathrm{inj}$", "S$_\mathrm{str}$", "S$_\mathrm{mtr}$","S$_\mathrm{ltr}$", ]

attributes = ["Calcite","Dolomite","Ankerite","Siderite","Albite","Clinochlore",
             "Illite","Kaolinite","Smectite","Na$^{+}$","K$^{+}$","Ca$^{2+}$","Mg$^{2+}$",
             "HCO$_{3}^{-}$","Fe$^{2+}$","pH","Porosity","Aq. CO$_2$","SMCO$_2$"]

time = np.arange(0, 201, 1)

files0m   = ["fitness_all0m.csv","H_all0m4.csv","W_all0m4.csv"]

a    = np.genfromtxt("results0m/" + files0m[0], delimiter=',', skip_header=0)

b    = np.genfromtxt("results0m/" + files0m[1], delimiter=',', skip_header=0).T
b    = np.hstack((b[:,1], b[:,3], b[:,0], b[:,2])).reshape(201, 4, order="F")
c    = np.genfromtxt("results0m/" + files0m[2], delimiter=',', skip_header=0)
c    = np.hstack((c[:,1], c[:,3], c[:,0], c[:,2])).reshape(19, 4, order="F")
#---------------------- Plot at Point 1 --------------------------------;

gs = gridspec.GridSpec(2, 2)

fig = plt.figure(figsize=(7.5,7.5))
ax1 = fig.add_subplot(gs[0, 0]) # row 0, col 0
ax1.plot(a[:,0], a[:,1]/max(a[:,1]), 'k', label="Reconstruction error")
ax1.plot(a[:,0], a[:,2], 'r', label="Silhouette width")
ax1.set_ylabel("Normalized (-)")
ax1.set_ylim(0, 1)
ax1.set_xlabel("Signal number")
ax1.legend()
ax1.grid(linestyle='solid')
ax1.set_title("(a)",loc="left")
ax1.set_xticks(np.linspace(2,12,11))
#ax1.set_yticks([0,5,10,20,30,40,50])

ax2 = fig.add_subplot(gs[1, 0]) # row 0, col 1
for i in range(b.shape[1]):
    ax2.plot(time, b[:,i]/max(b[:,i]), colors[i], label=signals[i], linestyle=linestyles[i])
    ax2.legend(loc=1)
    ax2.set_title("(b)",loc="left")
    ax2.set_ylabel("Normalized (-)")
    ax2.grid(which='both', linestyle='--')
    ax2.set_xlabel("Time (yr)")
    ax2.grid(linestyle='solid')


ax3 = fig.add_subplot(gs[:, 1]) # row 1, span all columns
ax3.set_title("(c)",loc="left")
sns.heatmap(c/np.amax(c, axis=0), cmap="rainbow", xticklabels=signals, yticklabels=attributes, 
                 cbar_kws={"shrink": 0.5}, linewidth=0.25, square=True, ax=ax3)

fig.tight_layout()
fig.savefig('figures_manuscript/nmfk0m.pdf')
fig.savefig('figures_manuscript/nmfk0m-presentation.png')

#%%
#---------------------- Plot at Point 2 --------------------------------;
#signals = ["S$_\mathrm{mtr}$","S$_\mathrm{inj}$","S$_\mathrm{ltr}$", "S$_\mathrm{str}$"]

files200m = ["fitness_all200m.csv","H_all200m4.csv","W_all200m4.csv"]

d    = np.genfromtxt("results200m/" + files200m[0], delimiter=',', skip_header=0)

e    = np.genfromtxt("results200m/" + files200m[1], delimiter=',', skip_header=0).T
e    = np.hstack((e[:,1], e[:,3], e[:,0], e[:,2])).reshape(201, 4, order="F")
f    = np.genfromtxt("results200m/" + files200m[2], delimiter=',', skip_header=0)
f    = np.hstack((f[:,1], f[:,3], f[:,0], f[:,2])).reshape(19, 4, order="F")

gs = gridspec.GridSpec(2, 2)

fig = plt.figure(figsize=(7.5,7.5))
ax1 = fig.add_subplot(gs[0, 0]) # row 0, col 0
ax1.plot(d[:,0], d[:,1]/max(d[:,1]), 'k', label="Reconstruction error")
ax1.plot(d[:,0], d[:,2], 'r', label="Silhouette width")
ax1.set_ylabel("Normalized (-)")
ax1.set_ylim(0, 1)
ax1.set_xlabel("Signal number")
ax1.legend()
ax1.grid(linestyle='solid')
ax1.set_title("(a)",loc="left")
ax1.set_xticks(np.linspace(2,12,11))
#ax1.set_yticks([0,5,10,20,30,40,50])

ax2 = fig.add_subplot(gs[1, 0]) # row 0, col 1
for i in range(e.shape[1]):
    ax2.plot(time, e[:,i]/max(e[:,i]), colors[i], label=signals[i], linestyle=linestyles[i])
    ax2.legend(loc=1)
    ax2.set_title("(b)",loc="left")
    ax2.set_ylabel("Normalized (-)")
    ax2.grid(which='both', linestyle='--')
    ax2.set_xlabel("Time (yr)")
    ax2.grid(linestyle='solid')

ax3 = fig.add_subplot(gs[:, 1]) # row 1, span all columns
ax3.set_title("(c)",loc="left")
sns.heatmap(f/np.amax(f, axis=0), cmap="rainbow", xticklabels=signals, yticklabels=attributes, 
                 cbar_kws={"shrink": 0.5}, linewidth=0.25, square=True, ax=ax3)

fig.tight_layout()
fig.savefig('figures_manuscript/nmfk200m.pdf')
fig.savefig('figures_manuscript/nmfk200m-presentation.png')

'''
cmap = plt.get_cmap('inferno',30)
cmap.set_under('white')#Colour values less than vmin in white
cmap.set_over('red')# colour valued larger than vmax in red

Crosstab=50000*np.random.randn(10,10)
#sns.heatmap(c/np.amax(c, axis=0), cmap=cmap, xticklabels=signals, yticklabels=attributes, 
#                 vmin=0.01,vmax=1, cbar_kws={"shrink": 0.5}, linewidth=0.25, square=True, linecolor="grey", ax=ax3)
'''