# -*- coding: utf-8 -*-
"""
URSA interpolation of data
This script reads data that are collected 
by Chris, fit them and create equal intervalled 
data
Date: 11/21/2019
"""
import numpy as np

variables = ["pH","Porosity","Aq. CO$_2$","SMCO$_2$"]

species  = ["Na$^{+}$","K$^{+}$","Ca$^{2+}$","Mg$^{2+}$","HCO$_{3}^{+}$","Fe$^{2+}$"]

minerals = ["Calcite","Dolomite","Ankerite","Siderite","Albite","Clinochlore","Illite","Kaolinite","Smectite"]

files0m   = ["mins_0m.csv","species_0m.csv","physio-chem_variables0m.csv"]

files200m = ["mins_200m.csv","species_200m.csv","physio-chem_variables200m.csv"]

a    = np.genfromtxt("data/original_data/" + files0m[0], delimiter=',', skip_header=1)

b    = np.genfromtxt("data/original_data/" + files0m[1], delimiter=',', skip_header=1)

c    = np.genfromtxt("data/original_data/" + files0m[2], delimiter=',', skip_header=1)

d    = np.genfromtxt("data/original_data/" + files200m[0], delimiter=',', skip_header=1)

e    = np.genfromtxt("data/original_data/" + files200m[1], delimiter=',', skip_header=1)

f    = np.genfromtxt("data/original_data/" + files200m[2], delimiter=',', skip_header=1)

all_dat0m   =  np.hstack((a, b, c))

all_dat200m =  np.hstack((d, e, f))

def minmax(data,outpfile):
    minimum  = []
    mintime = []
    maximum  = []
    maxtime = []
    for i in range(data.shape[1]):
        
        minimum.append(min(data[:,i]))
        
        minitime = np.where(min(data[:,i]) == data[:,i])[0]
        
        minitime1 = minitime[0]
        
        mintime.append(minitime1)
        
        maximum.append(max(data[:,i]))
        
        maxitime = np.array(np.where(max(data[:,i]) == data[:,i]))[0]
        
        maxitime1 = maxitime[0]
        
        maxtime.append(maxitime1)
        
    arr = np.hstack((np.array(minimum), np.array(mintime), np.array(maximum), np.array(maxtime))).reshape((19,4), order='F')
    np.savetxt(outpfile, arr, fmt="%2.6f,%d,%2.6f,%d")

minmax(all_dat0m,"data/original_data/Point1minmax.csv")
minmax(all_dat200m,"data/original_data/Point2minmax.csv")

print('!! DONE !!')
