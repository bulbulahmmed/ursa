#=
import Cairo
import Fontconfig
import StatsBase
import MultivariateStats
import Compose
import NMFk
import Gadfly
import DelimitedFiles
=#
#import Mads
#import Plotly
#import PlotlyJS

# File names
files     = ["physio-chem_variables0m.csv","physio-chem_variables200m.csv","mins_0m.csv","mins_200m.csv","species_0m.csv","species_200m.csv"]

mins      = ["calcite","dolomite","ankerite","siderite","quartz","albite","Clinochlore",
            "Illite","Kaolinite","Smectite"]

species   = ["Na","K","Ca","Mg","HCO3","Fe"]

variables = ["Pressure","Temperature","pH","Aq. CO2","SMCO2"]

attributes = ["Calcite","Dolomite","Ankerite","Siderite","Albite","Clinochlore","Illite","Kaolinite","Smectite","Na","K","Ca","Mg","HCO3","Fe","pH","Porosity","Aq. CO2","SMCO2"]

resultdir1 = "results0m"
figuredir1 = "figures0m"

resultdir2 = "results200m"
figuredir2 = "figures200m"

#---------------------- All attributes ----------------------;

X1, header1 = DelimitedFiles.readdlm("data/"*files[1], ','; header=true, skipstart=0)
X2, header2 = DelimitedFiles.readdlm("data/"*files[2], ','; header=true, skipstart=0)
X3, header3 = DelimitedFiles.readdlm("data/"*files[3], ','; header=true, skipstart=0)

X4, header4 = DelimitedFiles.readdlm("data/"*files[4], ','; header=true, skipstart=0)
X5, header5 = DelimitedFiles.readdlm("data/"*files[5], ','; header=true, skipstart=0)
X6, header6 = DelimitedFiles.readdlm("data/"*files[6], ','; header=true, skipstart=0)

#---------------------- Point 1: 0m ----------------------;
X_0 = hcat(X1,X3,X5)
X = permutedims(X_0)
Xu, nmin, nmax = NMFk.normalizematrix_row!(X)
nkrange = 2:12
W, H, fitquality, robustness, aic, kopt = NMFk.execute(Xu, nkrange; resultdir=resultdir1, load=false)

for i=2:12
  #Saving plots

  NMFk.plotmatrix(permutedims(W[i]')./maximum(W[i]'); 
  filename=figuredir1*"/all0m$i.png", vsize=6Compose.inch, 
  yticks=attributes, xticks=["S$j" for j=1:i], 
  gm=[Gadfly.Theme(minor_label_font_size=16Gadfly.pt, key_label_font_size=16Gadfly.pt, major_label_font="Arial")])
  
  # Saving data
  DelimitedFiles.writedlm(resultdir1*"/W_all0m$i.csv", W[i], ",")
  DelimitedFiles.writedlm(resultdir1*"/H_all0m$i.csv", H[i], ",")
end
DelimitedFiles.writedlm(resultdir1*"/fitness_all0m.csv", hcat(collect(nkrange),fitquality[2:end],robustness[2:end]), ",")

#---------------------- Point 2: 200m ----------------------;

X_200 = hcat(X2,X4,X6)
X = permutedims(X_200)
Xu, nmin, nmax = NMFk.normalizematrix_row!(X)
nkrange = 2:12
W, H, fitquality, robustness, aic, kopt = NMFk.execute(Xu, nkrange; resultdir=resultdir2, load=false)

for i=2:12
  #Saving plots
  NMFk.plotmatrix(permutedims(W[i]')./maximum(W[i]'); 
  filename=figuredir2*"/all200m$i.png", vsize=6Compose.inch, 
  yticks=attributes, xticks=["S$j" for j=1:i], 
  gm=[Gadfly.Theme(minor_label_font_size=16Gadfly.pt, key_label_font_size=16Gadfly.pt, major_label_font="Arial")])
  # Saving data
  DelimitedFiles.writedlm(resultdir2*"/W_all200m$i.csv", W[i], ",")
  DelimitedFiles.writedlm(resultdir2*"/H_all200m$i.csv", H[i], ",")
end

DelimitedFiles.writedlm(resultdir2*"/fitness_all200m.csv", hcat(collect(nkrange),fitquality[2:end],robustness[2:end]), ",")

#=
#---------------------- Physio-chemical variables ----------------------;

X, header = DelimitedFiles.readdlm("data/"*files[1], ','; header=true, skipstart=0)

X = permutedims(X)

Xu, nmin, nmax = NMFk.normalizematrix_row!(X)

nkrange = 2:6

W, H, fitquality, robustness, aic, kopt = NMFk.execute(Xu, nkrange; resultdir=resultdir, load=false)

NMFk.plotmatrix(permutedims(W[kopt]')./maximum(W[kopt]'); 
  filename="figures/variables.png", vsize=6Compose.inch, 
  yticks=variables, xticks=["S$i" for i=1:kopt], 
  gm=[Gadfly.Theme(minor_label_font_size=16Gadfly.pt, key_label_font_size=16Gadfly.pt, major_label_font="Arial")])

DelimitedFiles.writedlm(resultdir*"/W_var.csv", W[kopt], ",")
DelimitedFiles.writedlm(resultdir*"/H_var.csv", H[kopt], ",")
DelimitedFiles.writedlm(resultdir*"/fitness_var.csv", hcat(collect(nkrange),fitquality[2:end],robustness[2:end]), ",")

#---------------------- Minerals at 0m -----------------------;
mineral0m  = ["Siderite","Dolomite","Quartz","Smectite","Kaolinite"]

X, header = DelimitedFiles.readdlm("data/"*files[2], ','; header=true, skipstart=0)

X = permutedims(X[:,2:6])

Xu, nmin, nmax = NMFk.normalizematrix_row!(X)

nkrange = 2:6

W, H, fitquality, robustness, aic, kopt = NMFk.execute(Xu, nkrange; resultdir=resultdir, load=false)

NMFk.plotmatrix(permutedims(W[kopt]')./maximum(W[kopt]'); 
  filename="figures/minerals0m.png", vsize=6Compose.inch, 
  yticks=mineral0m, xticks=["S$i" for i=1:kopt], 
  gm=[Gadfly.Theme(minor_label_font_size=16Gadfly.pt, key_label_font_size=16Gadfly.pt, major_label_font="Arial")])

DelimitedFiles.writedlm(resultdir*"/W_min0m.csv", W[kopt], ",")
DelimitedFiles.writedlm(resultdir*"/H_min0m.csv", H[kopt], ",")
DelimitedFiles.writedlm(resultdir*"/fitness_min0m.csv", hcat(collect(nkrange),fitquality[2:end],robustness[2:end]), ",")

#---------------------- Minerals at 200m ---------------------;
mineral200m  = ["Siderite","Dolomite","Quartz","Smectite","Kaolinite"]

X, header = DelimitedFiles.readdlm("data/"*files[3], ','; header=true, skipstart=0)

X = permutedims(X[:,2:8])

Xu, nmin, nmax = NMFk.normalizematrix_row!(X)

nkrange = 2:6

W, H, fitquality, robustness, aic, kopt = NMFk.execute(Xu, nkrange; resultdir=resultdir, load=false)

NMFk.plotmatrix(permutedims(W[kopt]')./maximum(W[kopt]'); 
  filename="figures/minerals200m.png", vsize=6Compose.inch, 
  yticks=mineral200m, xticks=["S$i" for i=1:kopt], 
  gm=[Gadfly.Theme(minor_label_font_size=16Gadfly.pt, key_label_font_size=16Gadfly.pt, major_label_font="Arial")])

DelimitedFiles.writedlm(resultdir*"/W_min200m.csv", W[kopt], ",")
DelimitedFiles.writedlm(resultdir*"/H_min200m.csv", H[kopt], ",")
DelimitedFiles.writedlm(resultdir*"/fitness_min200m.csv", hcat(collect(nkrange),fitquality[2:end],robustness[2:end]), ",")

#---------------------- Species at 0m ------------------------;

X, header = DelimitedFiles.readdlm("data/"*files[4], ','; header=true, skipstart=0)

X = permutedims(X[:,2:7])

Xu, nmin, nmax = NMFk.normalizematrix_row!(X)

nkrange = 2:6

W, H, fitquality, robustness, aic, kopt = NMFk.execute(Xu, nkrange; resultdir=resultdir, load=false)

NMFk.plotmatrix(permutedims(W[kopt]')./maximum(W[kopt]'); 
  filename="figures/species0m.png", vsize=6Compose.inch, 
  yticks=species, xticks=["S$i" for i=1:kopt], 
  gm=[Gadfly.Theme(minor_label_font_size=16Gadfly.pt, key_label_font_size=16Gadfly.pt, major_label_font="Arial")])

DelimitedFiles.writedlm(resultdir*"/W_sp0m.csv", W[kopt], ",")
DelimitedFiles.writedlm(resultdir*"/H_sp0m.csv", H[kopt], ",")
DelimitedFiles.writedlm(resultdir*"/fitness_sp0m.csv", hcat(collect(nkrange),fitquality[2:end],robustness[2:end]), ",")

#---------------------- Species at 200m ----------------------;

X, header = DelimitedFiles.readdlm("data/"*files[5], ','; header=true, skipstart=0)

X = permutedims(X[:,2:7])

Xu, nmin, nmax = NMFk.normalizematrix_row!(X)

nkrange = 2:6

W, H, fitquality, robustness, aic, kopt = NMFk.execute(Xu, nkrange; resultdir=resultdir, load=false)

NMFk.plotmatrix(permutedims(W[kopt]')./maximum(W[kopt]'); 
  filename="figures/species200m.png", vsize=6Compose.inch, 
  yticks=species, xticks=["S$i" for i=1:kopt], 
  gm=[Gadfly.Theme(minor_label_font_size=16Gadfly.pt, key_label_font_size=16Gadfly.pt, major_label_font="Arial")])

DelimitedFiles.writedlm(resultdir*"/W_sp200m.csv", W[kopt], ",")
DelimitedFiles.writedlm(resultdir*"/H_sp200m.csv", H[kopt], ",")
DelimitedFiles.writedlm(resultdir*"/fitness_sp200m.csv", hcat(collect(nkrange),fitquality[2:end],robustness[2:end]), ",")

#=
X_sp200, h_sp200 = DelimitedFiles.readdlm(files[5], ','; header=true);
X_sp200 = X_sp200[:,2:size(X_sp200)[2]];
W_sp200, H_sp200, fitquality_sp200, robustness_sp200, aic_sp200, kopt_sp200 = NMFk.execute(X_sp200, 2:size(X_sp200)[2], 1000; resultdir="results_normalized");
DelimitedFiles.writedlm("W_sp200_opt.csv", W_min200[kopt_sp200], ",");
DelimitedFiles.writedlm("H_sp200_opt.csv", H_sp200[kopt_sp200], ",");
DelimitedFiles.writedlm("fit_robst_aic_sp200.csv", hcat(fitquality_sp200, robustness_sp200, aic_sp200), ",");
NMFk.plotmatrix(permutedims(H_sp200[kopt_sp200])./maximum(H_sp200[kopt_sp200]); 
  filename="Figures/species_200m.png", vsize=6Compose.inch, 
  xticks=["S$i" for i=1:kopt_sp200], yticks=species, 
  gm=[Gadfly.Theme(minor_label_font_size=16Gadfly.pt, key_label_font_size=16Gadfly.pt, major_label_font="Arial")])
=#


