#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 12 14:49:18 2020

@author: bulbulahmmed
This code generates plot for NMFk-CO2 sequestration work
Each plot will have four subplots
a. will include normalized data
b. Silhoutte vs Fitnees
c. W matrix as line map
d. H matrix as heatmap
"""

a    = np.genfromtxt("data/" + files[0], delimiter=',', skip_header=1)

W = np.genfromtxt("results/W_var.csv", delimiter=',')

H = np.genfromtxt("results/H_var.csv", delimiter=',')

fit = np.genfromtxt("results/fitness_var.csv", delimiter=',')

signals=["S1","S2","S3","S4"]

fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(7.5, 6.5))

#---------------------- Plot # a --------------------------------;
ax1.plot(time, a[:,0]/max(a[:,0]), 'c', label=variables[0])
ax1.plot(time, a[:,1]/max(a[:,1]), 'm', label=variables[1])
ax1.plot(time, a[:,2]/max(a[:,2]), 'r', label=variables[2])
ax1.plot(time, a[:,3]/max(a[:,3]), 'b', label=variables[3])
ax1.plot(time, a[:,4]/max(a[:,4]), 'k', label=variables[4])
ax1.plot(time, a[:,5]/max(a[:,5]), 'g', label=variables[5])
ax1.legend()
ax1.set_xlabel("Time (yr)")
ax1.set_ylabel("Normalized (-)")
ax1.grid(which='both', linestyle='--')

#---------------------- Plot # b --------------------------------;
ax2.plot(fit[:,0], fit[:,1], 'k', label="$\epsilon(k)$")
ax2.plot(fit[:,0], fit[:,2], 'r', label="Silhoutte width")
ax2.set_xlabel("Signal")
ax2.legend()
ax2.grid(linestyle='--')
ax2.set_yticks([0,1,5,10,15])
#---------------------- Plot # c --------------------------------;
ax3.plot(time, H[0,:], 'k', label=signals[0])
ax3.plot(time, H[1,:], 'g', label=signals[1])
ax3.plot(time, H[2,:], 'r', label=signals[2])
ax3.plot(time, H[2,:], 'b', label=signals[3])
ax3.set_xlabel("Time (yr)")
ax3.set_ylabel("Signal magnitude (-)")
ax3.grid(which='both', linestyle='--')
ax3.legend()
#---------------------- Plot # d --------------------------------;
sns.heatmap(W/np.amax(W, axis=0), xticklabels=signals, yticklabels=species, 
                 cmap="jet", linewidth=0.25, square=True, ax=ax4)

fig.tight_layout()

fig.savefig('figures/variables.pdf')


