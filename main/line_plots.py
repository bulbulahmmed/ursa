#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 12 14:49:18 2020

@author: bulbulahmmed
This code generates plot for NMFk-CO2 sequestration work
Each plot will have four subplots
a. will include normalized data
b. Silhoutte vs Fitnees
c. W matrix as line map
d. H matrix as heatmap
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
plt.rcParams['font.size'] = 11
plt.rcParams["font.family"] = "serif"
#plt.rcParams["font.family"] = "Arial"
plt.rcParams['mathtext.it'] = 'STIXGeneral:italic'
plt.rcParams['mathtext.bf'] = 'STIXGeneral:italic:bold'
#plt.rc('text', usetex=True)

min_colors = ['#1f77b4','#ff7f0e','#2ca02c','#d62728','#9467bd','#8c564b','#e377c2','#7f7f7f','#bcbd22']
min_linestyles = ['solid', 'dashed', 'dashdot','dotted','dashed', 'dashed', 'dashed','dashed','dashed']
min_marker = ['None', 'None', 'None','None','o', '^', '>','D','*']
var_colors = ['#1f77b4','#ff7f0e','#000000','#d62728']
var_linestyles = ['solid', 'dashed', 'dashdot','dotted']
var_marker = ['None', 'None', 'None','None','o', '^', '>','+','*']
sp_colors = ['#1f77b4','#ff7f0e','#2ca02c','#d62728','#9467bd','#8c564b']
sp_linestyles = ['solid', 'dashed', 'dashdot','dotted','solid', 'dashed']
sp_marker = ['None', 'None', 'None','None','>', '^']

variables = ["pH","Porosity","Aq. CO$_2$","SMCO$_2$"]

species  = ["Na$^{+}$","K$^{+}$","Ca$^{2+}$","Mg$^{2+}$","HCO$_{3}^{-}$","Fe$^{2+}$"]

minerals = ["Calcite","Dolomite","Ankerite","Siderite","Albite","Clinochlore","Illite","Kaolinite","Smectite"]

time = np.arange(0, 201, 1)

files0m   = ["mins_0m.csv","species_0m.csv","physio-chem_variables0m.csv"]

files200m = ["mins_200m.csv","species_200m.csv","physio-chem_variables200m.csv"]

a    = np.genfromtxt("data/" + files0m[0], delimiter=',', skip_header=1)

b    = np.genfromtxt("data/" + files0m[1], delimiter=',', skip_header=1)

c    = np.genfromtxt("data/" + files0m[2], delimiter=',', skip_header=1)

#---------------------- Plot at Point 1 --------------------------------;

fig, (ax1, ax2, ax3) = plt.subplots(3, 1, figsize=(6.5, 8.5))

#---------------------- Plot # a --------------------------------;
for i in range(a.shape[1]):
    ax1.plot(time, a[:,i]/max(a[:,i]), min_colors[i], label=minerals[i], linestyle=min_linestyles[i], marker=min_marker[i], markevery=20)
    ax1.legend(bbox_to_anchor=(1.01, 1), loc='upper left', borderaxespad=0.)
    ax1.set_title("(a)")
    ax1.set_ylabel("Normalized (-)")
    ax1.set_xticks(np.linspace(0,200,11))
    ax1.grid(which='both', linestyle='solid')

#---------------------- Plot # b --------------------------------;
for i in range(b.shape[1]):
    ax2.plot(time, b[:,i]/max(b[:,i]), sp_colors[i], label=species[i], linestyle=sp_linestyles[i], marker=sp_marker[i], markevery=20)
    ax2.legend(bbox_to_anchor=(1.01, 1), loc='upper left', borderaxespad=0.)
#    ax.set_xlabel("Time (yr)")
    ax2.set_ylabel("Normalized (-)")
    ax2.set_title("(b)")
    ax2.set_xticks(np.linspace(0,200,11))
    ax2.grid(which='both', linestyle='solid')

#---------------------- Plot # c --------------------------------;
for i in range(c.shape[1]):
    ax3.plot(time, c[:,i]/max(c[:,i]), var_colors[i], label=variables[i], linestyle=var_linestyles[i])
    ax3.legend(bbox_to_anchor=(1.01, 1), loc='upper left', borderaxespad=0.)
    ax3.set_xlabel("Time (yr)")
    ax3.set_ylabel("Normalized (-)")
    ax3.set_title("(c)")
    ax3.set_xticks(np.linspace(0,200,11))
    ax3.grid(which='both', linestyle='solid')

fig.tight_layout()

fig.savefig('figures_manuscript/data0m.pdf')
fig.savefig('figures_manuscript/data0m.png')

#%%
#---------------------- Plot at Point 2 --------------------------------;
d    = np.genfromtxt("data/" + files200m[0], delimiter=',', skip_header=1)

e    = np.genfromtxt("data/" + files200m[1], delimiter=',', skip_header=1)

f    = np.genfromtxt("data/" + files200m[2], delimiter=',', skip_header=1)

fig, (ax1, ax2, ax3) = plt.subplots(3, 1, figsize=(6.5, 8.5))

#---------------------- Plot # a --------------------------------;
for i in range(d.shape[1]):
    ax1.plot(time, d[:,i]/max(d[:,i]), min_colors[i], label=minerals[i], linestyle=min_linestyles[i], marker=min_marker[i], markevery=20)
    ax1.legend(bbox_to_anchor=(1.01, 1), loc='upper left', borderaxespad=0.)
    ax1.set_title("(a)")
    ax1.set_ylabel("Normalized (-)")
    ax1.set_xticks(np.linspace(0,200,11))
    ax1.grid(which='both', linestyle='solid')

#---------------------- Plot # b --------------------------------;
for i in range(e.shape[1]):
    ax2.plot(time, e[:,i]/max(e[:,i]), sp_colors[i], label=species[i], linestyle=sp_linestyles[i], marker=sp_marker[i], markevery=20)
    ax2.legend(bbox_to_anchor=(1.01, 1), loc='upper left', borderaxespad=0.)
#    ax.set_xlabel("Time (yr)")
    ax2.set_ylabel("Normalized (-)")
    ax2.set_title("(b)")
    ax2.set_xticks(np.linspace(0,200,11))
    ax2.grid(which='both', linestyle='solid')

#---------------------- Plot # c --------------------------------;
for i in range(f.shape[1]):
    ax3.plot(time, f[:,i]/max(f[:,i]), var_colors[i], label=variables[i], linestyle=var_linestyles[i])
    ax3.legend(bbox_to_anchor=(1.01, 1), loc='upper left', borderaxespad=0.)
    ax3.set_xlabel("Time (yr)")
    ax3.set_ylabel("Normalized (-)")
    ax3.set_title("(c)")
    ax3.set_xticks(np.linspace(0,200,11))
    ax3.grid(which='both', linestyle='solid')

fig.tight_layout()

fig.savefig('figures_manuscript/data200m.pdf')
fig.savefig('figures_manuscript/data200m.png')
