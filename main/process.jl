#
import Cairo
import Fontconfig
import StatsBase
import MultivariateStats
import Compose
import NMFk
import Gadfly
import DelimitedFiles
#
#import Mads
#import Plotly
#import PlotlyJS

# File names
files     = ["physio-chem_variables0m.csv","physio-chem_variables200m.csv","mins_0m.csv","mins_200m.csv","species_0m.csv","species_200m.csv"]

mins      = ["calcite","dolomite","ankerite","siderite","albite","Clinochlore",
            "Illite","Kaolinite","Smectite"]

species   = ["Na","K","Ca","Mg","HCO3","Fe"]

variables = ["Pressure","pH","Aq. CO2","SMCO2"]

attributes = ["Calcite","Dolomite","Ankerite","Siderite","Albite","Clinochlore","Illite","Kaolinite","Smectite","Na","K","Ca","Mg","HCO3","Fe","pH","Porosity","Aq. CO2","SMCO2"]

resultdir1 = "results0m"
figuredir1 = "figures0m"

resultdir2 = "results200m"
figuredir2 = "figures200m"

resultdir3 = "results0m-shift"
figuredir3 = "figures0m-shift"

resultdir4 = "results200m-shift"
figuredir4 = "figures200m-shift"

#---------------------- All attributes ----------------------;

X1, header1 = DelimitedFiles.readdlm("data/"*files[1], ','; header=true, skipstart=0)
X2, header2 = DelimitedFiles.readdlm("data/"*files[2], ','; header=true, skipstart=0)
X3, header3 = DelimitedFiles.readdlm("data/"*files[3], ','; header=true, skipstart=0)

X4, header4 = DelimitedFiles.readdlm("data/"*files[4], ','; header=true, skipstart=0)
X5, header5 = DelimitedFiles.readdlm("data/"*files[5], ','; header=true, skipstart=0)
X6, header6 = DelimitedFiles.readdlm("data/"*files[6], ','; header=true, skipstart=0)
#---------------------- Point 1: 0m ----------------------;
X_0 = hcat(X1,X3,X5)
X = permutedims(X_0)
Xu, nmin, nmax = NMFk.normalizematrix_row!(X)
nkrange = 2:12
W, H, fitquality, robustness, aic, kopt = NMFk.execute(Xu, nkrange; resultdir=resultdir1, load=false)

for i=2:12
  #Saving plots
  NMFk.plotmatrix(permutedims(W[i]')./maximum(W[i]'); 
  filename=figuredir1*"/all0m$i.png", vsize=6Compose.inch, 
  yticks=attributes, xticks=["S$j" for j=1:i], 
  gm=[Gadfly.Theme(minor_label_font_size=16Gadfly.pt, key_label_font_size=16Gadfly.pt, major_label_font="Arial")])  
  # Saving data
  DelimitedFiles.writedlm(resultdir1*"/W_all0m$i.csv", W[i], ",")
  DelimitedFiles.writedlm(resultdir1*"/H_all0m$i.csv", H[i], ",")
end

DelimitedFiles.writedlm(resultdir1*"/fitness_all0m.csv", hcat(collect(nkrange),fitquality[2:end],robustness[2:end]), ",")

#---------------------- Point 2: 200m ----------------------;
X_200 = hcat(X2,X4,X6)
X = permutedims(X_200)
Xu, nmin, nmax = NMFk.normalizematrix_row!(X)
nkrange = 2:12
W, H, fitquality, robustness, aic, kopt = NMFk.execute(Xu, nkrange; resultdir=resultdir2, load=false)

for i=2:12
  #Saving plots
  NMFk.plotmatrix(permutedims(W[i]')./maximum(W[i]'); 
  filename=figuredir2*"/all2000m$i.png", vsize=6Compose.inch, 
  yticks=attributes, xticks=["S$j" for j=1:i], 
  gm=[Gadfly.Theme(minor_label_font_size=16Gadfly.pt, key_label_font_size=16Gadfly.pt, major_label_font="Arial")])  
  # Saving data
  DelimitedFiles.writedlm(resultdir2*"/W_all2000m$i.csv", W[i], ",")
  DelimitedFiles.writedlm(resultdir2*"/H_all200m$i.csv", H[i], ",")
end

DelimitedFiles.writedlm(resultdir2*"/fitness_all200m.csv", hcat(collect(nkrange),fitquality[2:end],robustness[2:end]), ",")

#---------------------- Shift: Point 1: 0m ----------------------;
X_0 = hcat(X1,X3,X5)
X_0 .+= 0.0002
X = permutedims(X_0)
Xu, nmin, nmax = NMFk.normalizematrix_row!(X)
nkrange = 2:12
W, H, fitquality, robustness, aic, kopt = NMFk.execute(Xu, nkrange; resultdir=resultdir3, load=false)

for i=2:12
  #Saving plots
  NMFk.plotmatrix(permutedims(W[i]')./maximum(W[i]'); 
  filename=figuredir3*"/all0m$i.png", vsize=6Compose.inch, 
  yticks=attributes, xticks=["S$j" for j=1:i], 
  gm=[Gadfly.Theme(minor_label_font_size=16Gadfly.pt, key_label_font_size=16Gadfly.pt, major_label_font="Arial")])  
  # Saving data
  DelimitedFiles.writedlm(resultdir3*"/W_all0m$i.csv", W[i], ",")
  DelimitedFiles.writedlm(resultdir3*"/H_all0m$i.csv", H[i], ",")
end

DelimitedFiles.writedlm(resultdir3*"/fitness_all0m.csv", hcat(collect(nkrange),fitquality[2:end],robustness[2:end]), ",")

#---------------------- Shift: Point 2: 200m ----------------------;
X_200 = hcat(X2,X4,X6)
X_200 .+= 0.0002
X = permutedims(X_200)
Xu, nmin, nmax = NMFk.normalizematrix_row!(X)
nkrange = 2:12
W, H, fitquality, robustness, aic, kopt = NMFk.execute(Xu, nkrange; resultdir=resultdir4, load=false)

for i=2:12
  #Saving plots
  NMFk.plotmatrix(permutedims(W[i]')./maximum(W[i]'); 
  filename=figuredir4*"/all2000m$i.png", vsize=6Compose.inch, 
  yticks=attributes, xticks=["S$j" for j=1:i], 
  gm=[Gadfly.Theme(minor_label_font_size=16Gadfly.pt, key_label_font_size=16Gadfly.pt, major_label_font="Arial")])  
  # Saving data
  DelimitedFiles.writedlm(resultdir4*"/W_all2000m$i.csv", W[i], ",")
  DelimitedFiles.writedlm(resultdir4*"/H_all200m$i.csv", H[i], ",")
end

DelimitedFiles.writedlm(resultdir4*"/fitness_all200m.csv", hcat(collect(nkrange),fitquality[2:end],robustness[2:end]), ",")



