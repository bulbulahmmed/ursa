# -*- coding: utf-8 -*-
"""
URSA interpolation of data
This script reads data that are collected 
by Chris, fit them and create equally intervalled 
data
Date: 11/21/2019
"""
#import os
import numpy as np
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
#
input_file = "../data/1_ph_0m.csv"
data    = np.genfromtxt(input_file, delimiter=',',skip_header=0, skip_footer=0)
Na      = data[:, 0:2]
Na1      = Na[~np.isnan(Na)]
Na2     = np.reshape(Na1, (int(Na1.shape[0]/2), 2))
x1      = Na2[:,0]
y       = Na2[:,1]
y1      = np.where(y < 0, 0.00001, y)
x2      = np.arange(1,201,1)
plt.plot(x1, y1, '*') 
#*****************************************;
f   = interp1d(x1, y1, fill_value="extrapolate")
y2    = f(x2)
#y3    = np.where(y2 < 0, 0.00001, y2)
filename  = "2_temp_0m"
np.savetxt(filename + '.csv', np.vstack((x2, y2)).T, fmt="%d,%f",delimiter=',',
           header='Yr,Conc',comments='')
plt.plot(x1, y1, '*', x2, y2, '-') 
plt.xlabel('Time (yr)')  