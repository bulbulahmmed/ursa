# -*- coding: utf-8 -*-
"""
Created on Thu Jul 18 10:06:42 2019

@author: Christopher_Holle
"""
import re
import pandas as pd

# Parse Line function to check for feature
def parse_line(line):
    match = re.compile(r'float 1').search(line)
    if (match):
        feature = line.split()
        return feature[1]
    return None

data = pd.DataFrame()
with open('data_pflotran_data_mphase_v1-001.vtk') as file_object:
    line = file_object.readline()
    while line:
        # Check if the line contains a feature
        feature = parse_line(line)
        if feature:
            # Skip a line
            line = file_object.readline()
            new_values = []
            # 8 rows under every feature
            for index in range(8):
                line = file_object.readline()
                split_line = line.split()
                # 10 values in each row, add each to list
                for index in range(10):
                    new_values.append(split_line[index])
            # Add the new column with the appropriate header
            data[str(feature)] = new_values
        # Continue the loop
        line = file_object.readline()
        
# Add the Material_ID column
data['Material_ID'] = [1] * 80

data.to_csv('data_pflotran_data_mphase_v1-001_parsed.csv')
    