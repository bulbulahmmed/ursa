# -*- coding: utf-8 -*-
"""
URSA interpolation of data
This script reads data that are collected 
by Chris, fit them and create equally intervalled 
data
Date: 11/21/2019
"""
#import os
import numpy as np
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
#
mineral_dir  = "cleaned_data/Fig19_Volume_Change/"

mineral_dir2 = "cleaned_data/Fig21_Volume_Change_2/"

carb_min0m   = ["volume_change_a.csv","volume_change_b.csv","volume_change_c.csv","volume_change_d.csv"]

carb_min200m = ["volume_change_f.csv","volume_change_g.csv","volume_change_h.csv","volume_change_i.csv"]

noncarb_min0m   = ["volume_change_b.csv","volume_change_c.csv",
                   "volume_change_d.csv","volume_change_e.csv","volume_change_f.csv"]

noncarb_min200m = ["volume_change_h.csv","volume_change_i.csv",
                   "volume_change_j.csv","volume_change_k.csv","volume_change_l.csv"]

sp_dir = "cleaned_data/Fig23_Total_Aqueous_Concentrations/"

sp_0m = ["concentrations_a.csv","concentrations_b.csv","concentrations_c.csv","concentrations_d.csv",
         "concentrations_e.csv","concentrations_f.csv"]

sp_200m = ["concentrations_g.csv","concentrations_h.csv","concentrations_i.csv","concentrations_j.csv",
         "concentrations_k.csv","concentrations_l.csv"]

variables0m = ["cleaned_data/Fig11_pH/ph_a.csv",            
             "cleaned_data/Fig22_Porosity_Volume/porosity_a.csv",                         
             "cleaned_data/Fig14_Aqueous_CO2/aqueous_CO2_a.csv",
             "cleaned_data/Fig16_SMCO2/SMCO2_a.csv"] 

variables200m = ["cleaned_data/Fig11_pH/ph_c.csv",             
             "cleaned_data/Fig22_Porosity_Volume/porosity_b.csv",             
             "cleaned_data/Fig14_Aqueous_CO2/aqueous_CO2_b.csv",
             "cleaned_data/Fig16_SMCO2/SMCO2_b.csv"] 

mins = "Calcite,Dolomite,Ankerite,Siderite,Albite,Clinochlore,Illite,Kaolinite,Smectite"

species   = "Na,K,Ca,Mg,HCO3,Fe"

variable = "pH,Porosity,DissCO2,SMCO2"

#------------------------- Filenames ---------------------------;

minfile0 = "mins_0m.csv"

minfile200 = "mins_200m.csv"

sp0m = "species_0m.csv"

sp200m = "species_200m.csv"

phy_chem_vars0m = "physio-chem_variables0m.csv"

phy_chem_vars200m = "physio-chem_variables200m.csv"
#

def interpolation_data(inputfile):
    '''
    This function reads input files and generates 
    interpolated data for 200 years
    '''
    data    = np.genfromtxt(inputfile, delimiter=',',skip_header=0, skip_footer=0)
    x1      = data[:,0]
    y1       = data[:,1]
    x2      = np.arange(0,201,1)
    f   = interp1d(x1, y1, fill_value="extrapolate")
    y2    = f(x2)
    return x1, y1, x2, y2
'''------------------------- END of Function ------------------------'''
#************************ Execution ************************;
#----------------------- part 1: Min 0m ----------------------------;
min_zero = []
min_two_hundreds = []
for i in range(len(carb_min0m)):
    x1, y1, x2, y2 =  interpolation_data(mineral_dir + carb_min0m[i])
    min_zero.append(y2)
    x3, y3, x4, y4 =  interpolation_data(mineral_dir + carb_min200m[i])
    min_two_hundreds.append(y4)
    fig, ax=plt.subplots(figsize=(5.5,5.5))
    ax.plot(x1, y1, '*', label="Real")
    ax.plot(x2, y2, '-', label="Interp")
    ax.plot(x3, y3, '^', label="Real")
    ax.plot(x4, y4, '--', label="Interp")
    ax.get_legend()  

#--------------------------- part 2: Min 200m -----------------------------;
non_min_zero = []
non_min_two_hundreds = []
for i in range(len(noncarb_min0m)):
    x1, y1, x2, y2 =  interpolation_data(mineral_dir2 + noncarb_min0m[i])
    non_min_zero.append(y2)
    x3, y3, x4, y4 =  interpolation_data(mineral_dir2 + noncarb_min200m[i])
    non_min_two_hundreds.append(y4)
    fig, ax=plt.subplots(figsize=(5.5,5.5))
    ax.plot(x1, y1, '*', label="Real")
    ax.plot(x2, y2, '-', label="Interp")
    ax.plot(x3, y3, '^', label="Real")
    ax.plot(x4, y4, '--', label="Interp")
    ax.get_legend()  

#-------------------------- Part 1: Mineral 0m -----------------------------;    
min0 = np.array(min_zero).T 

minnoncarb0 = np.array(non_min_zero).T 

minall0m = np.hstack((min0, minnoncarb0))

minall0m1 = minall0m - np.nanmin(minall0m, axis=0) 

np.savetxt("../ursa_nmfk/data/original_data/" + minfile0, minall0m, fmt="%f",delimiter=',', header=mins,comments='')
np.savetxt("../ursa_nmfk/data/" + minfile0, minall0m1, fmt="%f",delimiter=',', header=mins,comments='')

#------------------------ Part 2: Mineral 200m ---------------------------;

min2 = np.array(min_two_hundreds).T 

minnoncarb2 = np.array(non_min_two_hundreds).T

minall200m = np.hstack((min2, minnoncarb2)) 

minall200m1 = minall200m - np.nanmin(minall200m, axis=0) 

np.savetxt("../ursa_nmfk/data/original_data/" + minfile200, minall200m, fmt="%f",delimiter=',', header=mins,comments='')
np.savetxt("../ursa_nmfk/data/" + minfile200, minall200m1, fmt="%f",delimiter=',', header=mins,comments='')

#--------------------------- part 3: Species -----------------------------;
sp_zero = []
sp_hundreds = []
for i in range(len(sp_0m)):
    x1, y1, x2, y2 =  interpolation_data(sp_dir + sp_0m[i])
    sp_zero.append(y2)
    x3, y3, x4, y4 =  interpolation_data(sp_dir + sp_200m[i])
    sp_hundreds.append(y4)
    fig, ax=plt.subplots(figsize=(5.5,5.5))
    ax.plot(x1, y1, '*', label="Real")
    ax.plot(x2, y2, '-', label="Interp")
    ax.plot(x3, y3, '^', label="Real")
    ax.plot(x4, y4, '--', label="Interp")
    ax.get_legend()  
    
sp_0 = np.array(sp_zero).T
 
sp_01 = sp_0 - np.nanmin(sp_0, axis=0)

sp_2 = np.array(sp_hundreds).T 

sp_02 = sp_2 - np.nanmin(sp_2, axis=0)

np.savetxt("../ursa_nmfk/data/original_data/" + sp0m, sp_0, fmt="%f", delimiter=',', header=species, comments='')
np.savetxt("../ursa_nmfk/data/" + sp0m, sp_01, fmt="%f", delimiter=',', header=species, comments='')

np.savetxt("../ursa_nmfk/data/original_data/" + sp200m, sp_2, fmt="%f", delimiter=',', header=species, comments='')
np.savetxt("../ursa_nmfk/data/" + sp200m, sp_02, fmt="%f", delimiter=',', header=species, comments='')
#--------------------------- part 4: variables at 0m-----------------------------;
physchemvar = []
for i in range(len(variables0m)):
    x1, y1, x2, y2 =  interpolation_data(variables0m[i])
    physchemvar.append(y2)
    fig, ax=plt.subplots(figsize=(5.5,5.5))
    ax.plot(x1, y1, '*', label="Real")
    ax.plot(x2, y2, '-', label="Interp")
    ax.get_legend()  
    
var0 = np.array(physchemvar).T

var01 = var0 - np.nanmin(var0, axis=0) 

np.savetxt("../ursa_nmfk/data/original_data/" + phy_chem_vars0m, var0, fmt="%f", delimiter=',', header=variable, comments='')
np.savetxt("../ursa_nmfk/data/" + phy_chem_vars0m, var01, fmt="%f", delimiter=',', header=variable, comments='')
#--------------------------- part 4: variables at 200m -----------------------------;
physchemvar200 = []
for i in range(len(variables200m)):
    x1, y1, x2, y2 =  interpolation_data(variables200m[i])
    physchemvar200.append(y2)
    fig, ax=plt.subplots(figsize=(5.5,5.5))
    ax.plot(x1, y1, '*', label="Real")
    ax.plot(x2, y2, '-', label="Interp")
    ax.get_legend()  
    
var200 = np.array(physchemvar200).T

var2001 = var200 - np.nanmin(var200, axis=0) 

np.savetxt("../ursa_nmfk/data/original_data/" + phy_chem_vars200m, var200, fmt="%f", delimiter=',', header=variable, comments='')
np.savetxt("../ursa_nmfk/data/" + phy_chem_vars200m, var2001, fmt="%f", delimiter=',', header=variable, comments='')
