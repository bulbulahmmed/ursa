# -*- coding: utf-8 -*-
"""
 This code will read data, create tensor for 11 mineral,
 and conduct Tucker-decomposition 
 Minerals are: Calcite, quartz, chlorite, illite, albite,
 siderite-2, ankerite, kaolinite, magnesite, dolomite, and
 smectite-na.
 Created on Sun Jun 16 14:15:09 2019

@author: Bulbul Ahmmed, Christopher Holle, and Scott James
"""
import time
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tensorly.decomposition import tucker, non_negative_tucker
import tensorly as tl

tic = time.clock()
#data_pd = pd.read_csv("react_data_bulbul.csv", delimiter=',')
#print(data_pd.columns)
data = np.genfromtxt("react_data_bulbul.csv", delimiter=',', skip_header=0)
print(data.shape)

# Clear 1st row of NaNs
data = np.delete(data, (0), axis=0)

# Clean negative values
print(np.any(data <= 0))
data[data<=0] = 0.00000001
print(np.any(data <= 0))

#%%
# ===============================================;
# This block will create array for every mineral ;
#================================================;
#params        = data[:, 0:48]  # Every columns excepts minerals
calcite       = data[:, 0:49]  # Input params plus calcite
quartz        = np.column_stack((data[:, 0:48], data[:, 49]))
chlorite      = np.column_stack((data[:, 0:48], data[:, 50]))
illite        = np.column_stack((data[:, 0:48], data[:, 51]))
albite        = np.column_stack((data[:, 0:48], data[:, 52]))
siderite_2    = np.column_stack((data[:, 0:48], data[:, 53]))
ankerite      = np.column_stack((data[:, 0:48], data[:, 54]))
kaolinite     = np.column_stack((data[:, 0:48], data[:, 55]))
magnesite     = np.column_stack((data[:, 0:48], data[:, 56]))
dolomite      = np.column_stack((data[:, 0:48], data[:, 57]))
smectite_na   = np.column_stack((data[:, 0:48], data[:, 58]))
#print(quartz.shape) # Checking if shape is alright
#print(quartz[1,48]) # Checking if value is of quartz
#print(smectite_na[1,48]) # Checking if value is for smectite_na
#%%
# ===============================================;
# This block will create tensor for every mineral;
#================================================;
tensor_array = np.array([calcite, quartz, chlorite, illite, albite, siderite_2, ankerite, kaolinite, magnesite, dolomite, smectite_na])
print(tensor_array.shape)
tensor = tl.tensor(tensor_array)
tl.context(tensor)

#%%
# ======================================================;
# This block will conduct Tucker-tensor decomposition   ;
#=======================================================;
tucker_rank = [5, 10000, 49]
random_state = 42
# Tucker decomposition
#core, tucker_factors = tucker(tensor, ranks=tucker_rank, init='random', tol=10e-5, random_state=random_state)
core, tucker_factors = non_negative_tucker(tensor, ranks=tucker_rank, rank=3, init='random', tol=10e-5, random_state=random_state)

#tucker_reconstruction = tl.tucker_to_tensor(core, tucker_factors)
print(core.shape)

toc = time.clock()
print("Total time taken by this process in sec :", tic -toc)
