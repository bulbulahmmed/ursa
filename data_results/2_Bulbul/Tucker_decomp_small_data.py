# -*- coding: utf-8 -*-
"""
 This code will read data, create tensor for 11 mineral,
 and conduct Tucker-decomposition 
 Minerals are: Calcite, quartz, chlorite, illite, albite,
 siderite-2, ankerite, kaolinite, magnesite, dolomite, and
 smectite-na.
 Created on Sun Jun 16 14:15:09 2019

@author: Bulbul Ahmmed, Christopher Holle, and Scott James
"""
import time
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tensorly.decomposition import tucker, non_negative_tucker
import tensorly as tl
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA, NMF
import seaborn as sns

sns.set()

tic = time.clock()
#data_pd = pd.read_csv("react_data_bulbul.csv", delimiter=',')
#print(data_pd.columns)
data = np.genfromtxt("react_data_bulbul.csv", delimiter=',', skip_header=0)
print(data.shape)

# Clear 1st row of NaNs
data = np.delete(data, (0), axis=0)

# Clean negative values
print(np.any(data <= 0))
data[data<=0] = 0.00000001
print(np.any(data <= 0))

data_1 = data[:5000,:59]
data_2 = data[37821:42821,:59]
data_small = np.append(data_1,data_2, axis=0)

#%%
# ===============================================;
# This block will create array for every mineral ;
#================================================;
#params        = data[:, 0:48]  # Every columns excepts minerals
calcite       = data_small[:, 0:49]  # Input params plus calcite
quartz        = np.column_stack((data_small[:, 0:48], data_small[:, 49]))
chlorite      = np.column_stack((data_small[:, 0:48], data_small[:, 50]))
illite        = np.column_stack((data_small[:, 0:48], data_small[:, 51]))
albite        = np.column_stack((data_small[:, 0:48], data_small[:, 52]))
siderite_2    = np.column_stack((data_small[:, 0:48], data_small[:, 53]))
ankerite      = np.column_stack((data_small[:, 0:48], data_small[:, 54]))
kaolinite     = np.column_stack((data_small[:, 0:48], data_small[:, 55]))
magnesite     = np.column_stack((data_small[:, 0:48], data_small[:, 56]))
dolomite      = np.column_stack((data_small[:, 0:48], data_small[:, 57]))
smectite_na   = np.column_stack((data_small[:, 0:48], data_small[:, 58]))
#print(quartz.shape) # Checking if shape is alright
#print(quartz[1,48]) # Checking if value is of quartz
#print(smectite_na[1,48]) # Checking if value is for smectite_na
#%%
# ===============================================;
# This block will create tensor for every mineral;
#================================================;
tensor_array = np.array([calcite, quartz, chlorite, illite, albite, siderite_2, ankerite, kaolinite, magnesite, dolomite, smectite_na])
print(tensor_array.shape)
tensor = tl.tensor(tensor_array)
tl.context(tensor)

#%%
# ======================================================;
# This block will conduct Tucker-tensor decomposition   ;
#=======================================================;
n_test = 5
tucker_rank = [n_test, 10000, 49]
random_state = 42
# Tucker decomposition
#core, tucker_factors = tucker(tensor, ranks=tucker_rank, init='random', tol=10e-5, random_state=random_state)
core, tucker_factors = non_negative_tucker(tensor, ranks=tucker_rank, rank=3, init='random', tol=10e-5, random_state=random_state)

#tucker_reconstruction = tl.tucker_to_tensor(core, tucker_factors)
print(core.shape)

toc = time.clock()
print("Total time taken by this process in sec :", tic -toc)


#%%

# Display Tucker factor W
print(tucker_factors[0])
print(tucker_factors[0].shape)

#%%

# ======================================================;
# This block will conduct k-means on tensor factor W    ;
#=======================================================;
kmeans = KMeans(n_clusters=n_test, random_state=random_state).fit(tucker_factors[0])
factor_labels = KMeans(n_clusters=n_test, random_state=random_state).fit_predict(tucker_factors[0])
print(factor_labels)

# ======================================================;
# This block will plot the output of k-means using PCA  ;
#=======================================================;
reduced_data = PCA(n_components=2).fit_transform(tucker_factors[0])
results = pd.DataFrame(reduced_data,columns=['pca_x','pca_y'])

sns.scatterplot(x="pca_x", y="pca_y", hue=factor_labels, data=results, palette=sns.color_palette("hls", n_test))
plt.title("PCA decomp")
plt.show()

# ======================================================;
# This block will plot the output of k-means using NMF  ;
#=======================================================;
reduced_data = NMF(n_components=2, init='random', random_state=random_state).fit_transform(tucker_factors[0])
results = pd.DataFrame(reduced_data,columns=['nmf_x','nmf_y'])

sns.scatterplot(x="nmf_x", y="nmf_y", hue=factor_labels, data=results, palette=sns.color_palette("hls", n_test))
plt.title("NMF decomp")
plt.show()
