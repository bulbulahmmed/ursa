#***************** Min and Max of variables *******************
ph_0m 4.681812 9.629683
pressure_0m 33170377.980772 33822012.244052
temp_0m 46.378285 77.409686
dissolved_CO2_0m 8.3e-05 0.080289
SMCO2_0m -3.743329 -0.623959
porosity_0m 0.125138 0.128398
#***************** Min and Max of species *******************
Na 0m 4e-05 0.031708
Mg 0m -8.8e-05 0.006165
K 0m 1.4e-05 0.000525
HCO3 0m 1e-05 0.67178
Fe 0m 0.0 0.001332
Ca 0m -0.000139 0.046731
# 200m
Na 200m 0.001256 0.04194
Mg 200m 1e-06 0.000302
K 200m 1.2e-05 0.00073
HCO3 200m 0.003578 0.051992
Fe 200m 0.0 5.1e-05
Ca 200m 2e-06 0.00715
#***************** Min and Max of Minerals *******************
siderite 0m 4.3e-05 0.00065
dolomite 0m -0.000156 0.001059
calcite 0m 1e-05 1e-05
ankerite 0m 1e-05 1e-05
quartz 0m 0.0 0.000725
smectite 0m 3e-06 0.000197
kaolinite 0m 4.1e-05 0.001025
illite 0m 1e-05 1e-05
chlinochlore 0m 1e-05 1e-05
albite 0m 1e-05 1e-05
# 200m
siderite 200m 4.4e-05 0.00075
dolomite 200m 3.2e-05 0.001919
calcite 200m 8.3e-05 0.001323
ankerite 200m 1e-05 1e-05
quartz 200m -3.7e-05 0.000798
smectite 200m 1.4e-05 0.000211
kaolinite 200m -0.000331 0.001201
illite 200m 1e-05 1e-05
chlinochlore 200m 1e-05 1e-05
albite 200m 1e-05 1e-05